/**
 * Dependencies
 */
import $ from 'jquery';
import 'normalize.css';
import { appDirective } from './app.directive';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngAnimate from 'angular-animate';
import ngMaterial from 'angular-material';
import ngMessages from 'angular-messages';
import ngReduxRouter from 'redux-ui-router';
/**
 * Redux dependencies
 */
import ngRedux from 'ng-redux';
import thunk from 'redux-thunk';
/**
 * Dev tools
 */
import ngReduxDevTools from 'ng-redux-dev-tools'; /**
 * Reducers
 */
import rootReducer from './reducers';
/**
 * Components
 */
import { home } from './components/home/home';
import { profile } from './components/profile/profile';
import { dashboard } from './components/dashboard/dashboard';
import { project } from './components/project/project';
import { tag } from './components/tag/tag';
import { navigation } from './components/navigation/navigation';
import { footer } from './components/footer/footer';
import { modal } from './components/modal/modal';
import { menuSidebar } from './components/menuSidebar/menuSidebar';
/**
 * Modal Specific Components
 */
import { signupModal } from './components/modal/views/signupModal/signupModal';
import { projectModal } from './components/modal/views/projectModal/projectModal';
import { tagModal } from './components/modal/views/tagModal/tagModal';
import { loginModal } from './components/modal/views/loginModal/loginModal';
/**
 * Sub Views
 */
import { selectionSheet } from './components/tag/views/selectionSheet/selectionSheet';
import { conditionSheet } from './components/tag/views/conditionSheet/conditionSheet';
/**
 * Shared Files
 */
 /*
  * Dispatches
  */
import * as SidebarActions from './actions/sidebar';
import * as ModalActions from './actions/modal';
import { shared } from './shared/shared';
angular.module('app', [
  uiRouter,
  ngAnimate,
  ngMaterial,
  ngMessages,
  ngRedux,
  ngReduxDevTools,
  ngReduxRouter,
  home.name,
  profile.name,
  dashboard.name,
  project.name,
  tag.name,
  navigation.name,
  footer.name,
  modal.name,
  loginModal.name,
  projectModal.name,
  signupModal.name,
  tagModal.name,
  selectionSheet.name,
  shared.name,
  menuSidebar.name,
  conditionSheet.name
])
.config(($ngReduxProvider, $mdThemingProvider) => {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue')
    .accentPalette('light-blue');

  $ngReduxProvider.createStoreWith(rootReducer, [thunk, 'ngUiRouterMiddleware']);
})
.run(($ngRedux, $rootScope) => {
  $ngRedux.dispatch(SidebarActions.initializeState());
  $ngRedux.dispatch(ModalActions.initializeState());

  // Binds ESC to close modal
  $(document).keyup((e) => {
    // Esc Key Code
    if (e.keyCode === 27) {
      $('.overlay, .modal').removeClass('fade-in');
      $('.overlay, .modal').addClass('fade-out');
      $ngRedux.dispatch(ModalActions.closeModal());
    }
  });

  // Binds closing modal on route change.
  $rootScope.$on('$stateChangeStart', (event, location, params) => {
    $ngRedux.dispatch(ModalActions.closeModal());
  });
})
.directive('app', appDirective);

