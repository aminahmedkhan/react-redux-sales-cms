import './projectModal.scss';
import { ProjectModalController as controller } from './projectModal.controller';
import template from './projectModal.html';

export const projectModalDirective = () => {
  return {
    controller,
    template,
    controllerAs: 'vm',
    scope: {},
    replace: true,
    restrict: 'E'
  };
};
