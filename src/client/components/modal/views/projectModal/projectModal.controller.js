import { bindActionCreators } from 'redux';
import { modalSelector } from '../../../../selectors/modal';
import * as ModalActions from '../../../../actions/modal';
import { stateGo } from 'redux-ui-router';

class ProjectModalController {
  constructor(Modal, Project, $ngRedux) {
    this.greeting = 'ProjectModalController!';
    this.Modal = Modal;
    this.Project = Project;
    this.$ngRedux = $ngRedux;

    this.$ngRedux.connect(modalSelector, this.mapDispatchToProps)(this);
  }

  create(project){
    this.Project.create(project)
      .then(({ data }) => {
        const { id } =  data; 
        this.closeModal();
        this.stateGo('project', { projectId: id })
      });
  }

  mapDispatchToProps(dispatch) {
    return bindActionCreators({
      ...ModalActions,
      stateGo
    }, dispatch);
  }
}

ProjectModalController.$inject = ['Modal', 'Project', '$ngRedux'];

export { ProjectModalController };
