import { projectModalDirective } from './projectModal.directive';
import angular from 'angular';


export const projectModal = angular.module('projectModal', [])
  .directive('projectModal', projectModalDirective);
