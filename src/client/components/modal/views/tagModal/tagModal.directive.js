import './tagModal.scss';
import { TagModalController as controller } from './tagModal.controller';
import template from './tagModal.html';

export const tagModalDirective = ()=> {
  return {
    controller,
    template,
    controllerAs: 'vm',
    scope: {},
    replace: true,
    restrict: 'E'
  };
};
