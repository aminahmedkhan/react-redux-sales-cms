import { tagModalDirective } from './tagModal.directive';
import uiRouter from 'angular-ui-router';
import angular from 'angular';

export const tagModal = angular.module('tagModal', [ uiRouter ])
  .directive('tagModal', tagModalDirective);
