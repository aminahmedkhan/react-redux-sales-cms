import { bindActionCreators } from 'redux';
// Selectors
import { devicesSelector } from '../../../../selectors/units';
// Actions
import * as ModalActions from '../../../../actions/modal';
import * as RouterActions from 'redux-ui-router';
import * as UnitActions from '../../../../actions/units';
class TagModalController {constructor(Tag, Modal, $stateParams, $ngRedux, $scope, $http, API) {
  this.Tag = Tag;
  this.Modal = Modal;
  this.$stateParams = $stateParams;
  this.$ngRedux = $ngRedux;
  this.$scope = $scope;
  this.$http = $http;
  const unsubscribe = this.$ngRedux.connect(
    this.mapStateToController,
    this.mapDispatchToController
  )(this);
  this.$http.get(`${API.url}${API.units}/devices`).then(({ data }) => {
    this.loadDevices(data);
  });
  this.$scope.$on('destroy', unsubscribe);
}

  create(tag) {
    /**
     * todo
     * uniform camelCase for intead of snake
     */
    tag.project_id = this.$stateParams.id;
    this.Tag.create(tag)
      .then(({ data }) => {
        const projectId = data.id;
        const tagId = data.tag_id;
        this.closeModal();
        this.stateGo('tag', { tagId, projectId });
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  mapStateToController(state) {
    return {
      devices: devicesSelector(state)
    };
  }

  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...ModalActions,
      ...RouterActions,
      ...UnitActions
    }, dispatch);
  }
}

TagModalController.$inject = ['Tag', 'Modal', '$stateParams', '$ngRedux', '$scope', '$http', 'API'];

export { TagModalController };
