import './loginModal.scss';
import { LoginModalController as controller } from './loginModal.controller';
import template from './loginModal.html';

export const loginModalDirective = () => {
  return {
    controller,
    template,
    controllerAs: 'vm',
    scope: {},
    replace: true,
    restrict: 'E'
  };
};
