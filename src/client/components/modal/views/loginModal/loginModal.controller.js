import { bindActionCreators } from 'redux';
import * as UserActions from '../../../../actions/user';
import * as ModalActions from '../../../../actions/modal';

class LoginModalController {
  constructor($ngRedux, $scope) {
    this.$ngRedux = $ngRedux;
    this.$scope = $scope;
    const unsubscribe = this.$ngRedux.connect(() => ({}), this.mapDispatchToController)(this);
    this.$scope.$on('destroy', unsubscribe);
  }

  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...UserActions,
      ...ModalActions
    }, dispatch);
  }
}

LoginModalController.$inject = [
  '$ngRedux',
  '$scope'
];

export { LoginModalController };
