import { loginModalDirective } from './loginModal.directive';
import angular from 'angular';
import uiRouter from 'angular-ui-router';


export const loginModal = angular.module('loginModal', [uiRouter])
  .config(($stateProvider) => {
    $stateProvider.state('loginModal', {
      template: '<loginModal></loginModal>'
    });
  })
  .directive('loginModal', loginModalDirective);
