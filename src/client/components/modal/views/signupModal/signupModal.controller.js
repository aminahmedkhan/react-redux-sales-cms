import { bindActionCreators } from 'redux';
import * as UserActions from '../../../../actions/user';
import * as ModalActions from '../../../../actions/modal';

class SignupModalController {
  constructor($scope, $ngRedux) {
    this.$scope = $scope;
    this.$ngRedux = $ngRedux;
    const unsubscribe = this.$ngRedux.connect(() => ({}), this.mapDispatchToController)(this);
    this.$scope.$on('destroy', unsubscribe);
  }

  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...UserActions,
      ...ModalActions
    }, dispatch);
  }
}

SignupModalController.$inject = [
  '$scope',
  '$ngRedux'
];

export { SignupModalController };
