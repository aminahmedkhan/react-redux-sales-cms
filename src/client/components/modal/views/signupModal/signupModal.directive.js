import './signupModal.scss';
import { SignupModalController as controller } from './signupModal.controller';
import template from './signupModal.html';

export const signupModalDirective = () => {
  return {
    controller,
    template,
    controllerAs: 'vm',
    scope: {},
    replace: true,
    restrict: 'E'
  };
};
