import { signupModalDirective } from './signupModal.directive';
import angular from 'angular';
import uiRouter from 'angular-ui-router';


export const signupModal = angular.module('signupModal', [uiRouter])
  .config(($stateProvider) => {
    $stateProvider.state('signupModal', {
      template: '<signupModal></signupModal>'
    });
  })
  .directive('signupModal', signupModalDirective);
