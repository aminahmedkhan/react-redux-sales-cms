import './modal.scss';
import { ModalController as controller } from './modal.controller';
import template from './modal.html';

export const modalDirective = () => {
  return {
    template,
    controller,
    controllerAs: 'vm',
    scope: {},
    restrict: 'E',
    replace: true
  };
};
