import angular from 'angular';
import uiRouter from 'angular-ui-router';
import { modalDirective } from './modal.directive';

export const modal = angular.module('modal', [
  uiRouter
])

.config(($stateProvider) => {
  $stateProvider.state('modal', {
    template: '<modal></modal>'
  });
})

.directive('modal', modalDirective);
