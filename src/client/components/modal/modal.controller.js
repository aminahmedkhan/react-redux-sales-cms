import { modalTemplateSelector } from '../../selectors/modal';
import { bindActionCreators } from 'redux';
import * as ModalActions from '../../actions/modal';
import * as UserActions from '../../actions/user';

class ModalController {
  constructor($ngRedux, $scope) {
    this.$ngRedux = $ngRedux;
    this.$scope = $scope;
    this.mapStateToController = this.mapStateToController.bind(this);
    const unsubscribe = this.$ngRedux.connect(this.mapStateToController, this.mapDispatchToController)(this);
    this.$scope.$on('destroy', unsubscribe);
  }

  mapStateToController(state) {
    return {
      template: modalTemplateSelector(state)
    };
  }
  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...ModalActions,
      ...UserActions
    }, dispatch);
  }
}

ModalController.$inject = [
  '$ngRedux',
  '$scope'
];

export { ModalController };
