import angular from 'angular';
import { footerDirective } from './footer.directive';

export const footer = angular.module('footer', [])
    .directive('footer', footerDirective);
