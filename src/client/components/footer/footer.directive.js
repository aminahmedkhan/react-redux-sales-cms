import './footer.scss';
import { FooterController as controller } from './footer.controller';
import template from './footer.html';

export const footerDirective = () => {
    return{
        template,
        controller,
        controllerAs: 'vm',
        restrict: 'E',
        replace: true,
        scope: {}
    };
};
