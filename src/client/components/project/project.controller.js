import * as ModalActions from '../../actions/modal';
import * as TagActions from '../../actions/tags';
import { stateGo } from 'redux-ui-router';
// import { modalSelector } from '../../selectors/modal';
import { routerSelector } from '../../selectors/router';
import { bindActionCreators } from 'redux';

class ProjectController {
  constructor(Modal, Project, Tag, $stateParams, $http, $ngRedux) {
    this.Modal = Modal;
    this.Tag = Tag;
    this.Project = Project;
    this.$stateParams = $stateParams;
    this.$http = $http;
    this.$ngRedux = $ngRedux;
    this.getProject($stateParams.projectId);
    this.getTags($stateParams.projectId);
    this.$ngRedux.connect(this.mapStateToController, this.mapDispatchToController)(this);
  }

  requestModal(view) {
    this.Modal.requestModal(view);
  }

  destroyModal() {
    this.Modal.destroyModal();
  }

  getProject(projectId) {
    this.$http.defaults.headers.common.Authorization = sessionStorage.getItem('token');
    this.Project.getOne(projectId)
      .then(({ data }) => {
        console.log('data', data);
        this.project = data;
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  getTags(id) {
    this.Tag.get(id)
      .then(() => {
        this.tags = this.Tag.getState();
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  openTag(tagId) {
    const projectId = this.router.currentParams.projectId;
    this.stateGo('tag', { projectId, tagId });
  }

  mapStateToController(state) {
    return {
      // modal: modalSelector(state),
      router: routerSelector(state)
    };
  }

  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...ModalActions,
      ...TagActions,
      stateGo
    }, dispatch);
  }

}

ProjectController.$inject = ['Modal', 'Project', 'Tag', '$stateParams', '$http', '$ngRedux'];

export { ProjectController };
