import angular from 'angular';
import { projectDirective } from './project.directive';

// placing an export in front of ar var is the same
// as exporting the var at the end of the file
// using export {varname}
export const project = angular.module('project', [])
.config( function ($stateProvider, $urlRouterProvider, $locationProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('project', {
    url: '/project/:projectId',
    template: '<project></project>',
    resolve:{
      projectId: ['$stateParams', function($stateParams){
        return $stateParams.projectId;
      }]
    }
  });
  $locationProvider.html5Mode({ enabled: true, requireBase: false });
})
.directive('project', projectDirective);
