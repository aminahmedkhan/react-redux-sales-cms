import './project.scss';
import { ProjectController as controller } from './project.controller';
import template from './project.html';

export const projectDirective = () => {
  return {
    template,
    controller,
    controllerAs: 'vm',
    restrict: 'E',
    replace: true,
    scope: {}
  };
};
