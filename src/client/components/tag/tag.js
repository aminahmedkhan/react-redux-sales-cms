import * as UnitActions from '../../actions/units';
import * as DatasheetActions from '../../actions/datasheet';
import angular from 'angular';
import { tagDirective } from './tag.directive';

// placing an export in front of ar var is the same
// as exporting the var at the end of the file
// using export {varname}
export const tag = angular.module('tag', [])
.config(($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('tag', {
    url: '/projectId/:projectId/tag/:tagId',
    template: '<tag></tag>',
    controller: (units, $ngRedux, $stateParams) => {
      units.tagId = $stateParams.tagId;
      $ngRedux.dispatch(DatasheetActions.addDeviceName($stateParams.tagId));
      $ngRedux.dispatch(DatasheetActions.loadDatasheetFields(units));
    },
    resolve: {
      units: (Units) => {
        return Units.get().then(() => {
          return Units.getTagUnits();
        });
      }
    }
  });
})
.directive('tag', tagDirective);
