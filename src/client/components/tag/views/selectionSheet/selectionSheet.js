import { selectionSheetDirective } from './selectionSheet.directive';
import angular from 'angular';
import uiRouter from 'angular-ui-router';


export const selectionSheet = angular.module('selectionSheet', [uiRouter])
.directive('selectionSheet', selectionSheetDirective);
