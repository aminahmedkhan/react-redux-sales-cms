import './selectionSheet.scss';
import { SelectionSheetController as controller } from './selectionSheet.controller';
import template from './selectionSheet.html';

export const selectionSheetDirective = () => {
  return {
    controller,
    template,
    controllerAs: 'vm',
    scope: {
      units: '='
    },
    replace: true,
    restrict: 'E'
  };
};
