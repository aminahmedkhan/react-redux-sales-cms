import { bindActionCreators } from 'redux';
// Selectors
import {
  datasheetSelector,
  deviceNameSelector,
  deviceImageSelector
} from 'client/selectors/datasheet';
// Actions

import * as DatasheetActions from 'client/actions/datasheet';

/**
    * TODO:
    *  Create mapControllerToProps to include:

    *    filterDropdowns,
    *    filterOrificeSizes
    */
class SelectionSheetController {
  constructor(Units, $rootScope, $scope, $ngRedux) {
    this.Units = Units;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$ngRedux = $ngRedux;

    this.MOCK_CALCULATED_DATA = 10;
    this.calculatedArea = this.MOCK_CALCULATED_DATA;
    this.selectedArea = this.MOCK_CALCULATED_DATA;
    // #TODO: Move to helpers file to be shared across multiple controllers
    this.booleanValues = [{ text: 'True', value: true }, { text: 'False', value: false }];
    const unsubscribe = this.$ngRedux.connect(
      this.mapStateToController,
      this.mapDispatchToController
    )(this);
    this.$scope.$on('destroy', unsubscribe);
  }

  /**
   * Returns filtered orifice sizes.
   * /shared/datasheet.js contains further detail on this method
   * @return {[Array of Objects]} Orifice Sizes
   */
  filterOrificeSizes(valveOption) {
    this.filteredOrificeSizes = this.Datasheet.filterOrificeSizes(valveOption);
  }

  recalculateDropdowns(size) {
    const { id } = JSON.parse(size);
    this.filteredValves = this.Datasheet.filterInletOutletDropdowns(id);
  }

  mapStateToController(state) {
    return {
      datasheet: datasheetSelector(state),
      deviceName: deviceNameSelector(state),
      deviceImage: deviceImageSelector(state)
    };
  }

  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...DatasheetActions
    }, dispatch);
  }

}

SelectionSheetController.$inject = ['Units', '$rootScope', '$scope', '$ngRedux'];

export { SelectionSheetController };
