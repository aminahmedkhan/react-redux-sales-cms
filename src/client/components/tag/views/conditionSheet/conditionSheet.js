import { conditionSheetDirective } from './conditionSheet.directive';
import angular from 'angular';
import uiRouter from 'angular-ui-router';


export const conditionSheet = angular.module('conditionSheet', [uiRouter])
  .directive('conditionSheet', conditionSheetDirective);
