import * as DatasheetActions from '../../../../actions/datasheet';
import { bindActionCreators } from 'redux';
import {
  overpressureSelector,
  datasheetSelector,
  totalBackPressureSelector
} from '../../../../selectors/datasheet';

class ConditionSheetController {
  constructor(Units, $rootScope, $ngRedux, $scope) {
    this.Units = Units;
    this.$ngRedux = $ngRedux;
    this.$rootScope = $rootScope;
    this.getUnits();
    this.TOTAL_BACK_PRESSURE = 5;
    const unsubscribe = this.$ngRedux.connect(this.mapStateToController, this.mapDispatchToController)(this);
    $scope.$on('$destroy', unsubscribe);
  }

  /**
   * [processDatasheet]
   * Sends data to backend (TODO: INPUT ROUTE HERE)
   * to manage form input data
   * @param  {[Object]} data [Data containing all form fields]
   * @return {[Object]}      [Returns data to process on selection sheet]
   */
  processDatasheet(data) {
    this.$rootScope.$broadcast('template:update', 'selectionSheet');
  }

  getUnits() {
    const units = this.Units.getTagUnits();
    if (units.length === 0) {
      this.Units.get()
      .then(() => {
        this.tagUnits = this.Units.getTagUnits();
      });
    } else {
      this.tagUnits = units;
    }
  }

  mapStateToController(state) {
    return {
      overpressure: overpressureSelector(state),
      datasheet: datasheetSelector(state),
      totalBackPressure: totalBackPressureSelector(state)
    }
  }

  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...DatasheetActions
    }, dispatch);
  }
}


ConditionSheetController.$inject = ['Units', '$rootScope', '$ngRedux', '$scope'];

export { ConditionSheetController };
