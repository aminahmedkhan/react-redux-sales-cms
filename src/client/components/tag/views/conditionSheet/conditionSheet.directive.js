import './conditionSheet.scss';
import { ConditionSheetController as controller } from './conditionSheet.controller';
import template from './conditionSheet.html';

export const conditionSheetDirective = ()=> {
  return {
    controller,
    template,
    controllerAs: 'vm',
    scope: {},
    replace: true,
    restrict: 'E'
  };
};
