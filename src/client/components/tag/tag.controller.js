import * as DatasheetActions from '../../actions/datasheet/';
import { datasheetSelector } from '../../selectors/datasheet';
import { bindActionCreators } from 'redux';

class TagController {
  constructor(Modal, Project, Tag, $stateParams, $http, $state, $ngRedux) {
    this.Modal = Modal;
    this.Project = Project;
    this.Tag = Tag;
    this.$stateParams = $stateParams;
    this.$http = $http;
    this.$state = $state;
    this.$ngRedux = $ngRedux;
    this.$ngRedux.connect(datasheetSelector, this.mapStateToController)(this);
    this.getProject($stateParams.projectId);
    this.getTags($stateParams.tagId);
  }

  getProject(projectId) {
    this.$http.defaults.headers.common['Authorization'] = sessionStorage.getItem('token');
    this.Project.getOne(projectId)
      .then(({ data }) => {
        this.projectData = data;
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  getTags(tagId) {
    this.Tag.getOne(tagId)
      .then((tag) => {
        this.tagData = tag;
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  mapStateToController(dispatch) {
    return bindActionCreators({
      ...DatasheetActions
    }, dispatch);
  }
}

TagController.$inject = [
  'Modal',
  'Project',
  'Tag',
  '$stateParams',
  '$http',
  '$state',
  '$ngRedux'
];

export { TagController };
