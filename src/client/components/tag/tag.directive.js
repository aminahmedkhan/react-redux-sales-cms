import './tag.scss';
import { TagController as controller } from './tag.controller';
import template from './tag.html';

export const tagDirective = () => {
  return{
    template,
    controller,
    controllerAs: 'vm',
    restrict: 'E',
    scope: {
      units: '='
    },
    replace: true
  };
};
