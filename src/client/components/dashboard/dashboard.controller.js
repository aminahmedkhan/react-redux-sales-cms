import { bindActionCreators } from 'redux';
import * as ModalActions from '../../actions/modal';
import * as ProjectActions from '../../actions/projects';
import * as RouterActions from 'redux-ui-router';

class DashboardController {
  constructor(Modal, Project, $ngRedux) {
    this.Modal = Modal;
    this.Project = Project;
    this.$ngRedux = $ngRedux;
    this.getProjects();
    this.$ngRedux.connect(null, this.mapDispatchToController)(this);
  }

  requestModal(view) {
    this.Modal.requestModal(view);
  }

  destroyModal() {
    this.Modal.destroyModal();
  }

  getProjects() {
    this.Project.get()
      .then(() => {
        this.projects = this.Project.getState();
      });
  }

  openProject(id) {
    this.stateGo('project', { projectId: id });
  }

  mapDispatchToController(dispatch) {
    return bindActionCreators({
      ...RouterActions,
      ...ModalActions,
      ...ProjectActions
    }, dispatch);
  }
}

DashboardController.$inject = ['Modal', 'Project', '$ngRedux'];

export { DashboardController };
