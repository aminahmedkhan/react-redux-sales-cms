import { dashboardDirective } from './dashboard.directive';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import { isAuthenticated } from '../../shared/authorization.js';

export const dashboard = angular.module('dashboard', [uiRouter])
  .config(($stateProvider) => {
    $stateProvider.state('dashboard', {
      url: '/dashboard',
      template: '<dashboard></dashboard>'
      // Uncomment when isAuthenticated is fixed
      // resolve: {
      //   isAuthenticated
      // }
    });
  })
  .directive('dashboard', dashboardDirective);
