import _ from 'lodash';
const projects = ($http, API, $q, $rootScope, $state) => {

  let allProjects = [];

/**
 * TODO
 *
 * Rename projects route to just '/'
 */

  const get = () => {
    const userId = sessionStorage.getItem('userId')
    return $http.get(`${API.url}/projects/`, { params: {userId} })
      .then((projects) => {
        console.log('projects', projects);
        allProjects = projects.data;
      });
  };

/**
 * Todo
 * Refactor route to user :id paramters
 *
 */
  const getOne = (projectId) => {
    const project = _.find(allProjects, (project) => {
      return project.id === Number(projectId);
    });

    //memoize
    if(project){
      return $q.when({ data: project });
    }else{
      return $http.get(`${API.url}/projects/${projectId}`);
    }
  };

  const getState = () => {
    return allProjects;
  };

  const create = (project) => {
    project.user_id = sessionStorage.getItem('userId');
    return $http.post(`${API.url}/projects/`, project);
  };

  const open = (projectId) => {
    $state.go('project', { projectId });
  };

  return{
    get,
    getState,
    getOne,
    create,
    open
  };
};


projects.$inject = ['$http', 'API', '$q', '$rootScope', '$state'];

export { projects };
