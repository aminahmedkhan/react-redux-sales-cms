import _ from 'lodash';
const modal = ($http, API, $q, $rootScope) => {
  const allModals = [
    { name: 'addProject', url: '/app/components/modal/views/project/project.html' },
    { name: 'addTag', url: '/app/components/modal/views/tagModal/tagModal.html' },
    { name: 'register', url: '/app/components/modal/views/registrationModal/registrationModal.html' },
    { name: 'login', url: '/app/components/modal/views/login-modal.html' }
  ];

  const modalListener = {
    update: 'modal:update',
    destroy: 'modal:destroy'
  };

  let currentModal = '';

  const destroyModal = () => {
    currentModal = '';
    $rootScope.$broadcast(modalListener.destroy);
  };

  const requestModal = (requestedView) => {
    if (currentModal) {
      destroyModal();
      return;
    }
    const viewFound = _.filter(allModals, (template) => {
      return template.name === requestedView;
    })[ 0 ];
    if (viewFound) {
      currentModal = viewFound;
      $rootScope.$broadcast(modalListener.update, currentModal);
    }
  };

  const getModal = () => {
    return currentModal;
  };

  return {
    requestModal,
    getModal,
    destroyModal
  };
};

modal.$inject = ['$http', 'API', '$q', '$rootScope'];

export { modal };
