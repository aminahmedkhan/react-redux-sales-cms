import _ from 'lodash';

const units = ($http, API, $q) => {
  let tagUnits = [];
  const get = () => {

    return $http.get(`${API.url}${API.units}`)
      .then(({ data }) => {
        tagUnits = data;
      });
  };

  const getTagUnits = () => {
    return tagUnits;
  };

  return {
    get,
    getTagUnits
  };
};

units.$inject = ['$http', 'API', '$q'];

export { units };
