const tags = ($http, API, $q, $rootScope, $state, $stateParams) => {

  let allTags = [];

  const get = ( id ) => {
    return $http.get(`${API.url}/tags/`, { params: { projectId: id } } )
      .then(( tags ) => {
        allTags = tags.data;
      });
  };

  const getState = () => {
    return allTags;
  };

  const getOne = ( tagId ) => {
    const tag = _.find(allTags, ( currentTag ) => {
      return currentTag.tagId === Number( tagId );
    });

    if (tag) {
      return $q.when({ data: tag });
    } else {
      return $http.get(`${API.url}/tags/`, { params: { tagId } });
    }

  };

  const create = ( tag ) => {
    tag.projectId = $stateParams.projectId;
    return $http.post(`${API.url}/tags/`, tag);
  };

  const open = ( tagId, projectId ) => {
    $state.go('tag', { tagId, projectId });
  };

  return{
    get,
    getState,
    getOne,
    create,
    open
  };
};


tags.$inject = ['$http', 'API', '$q', '$rootScope', '$state', '$stateParams'];

export { tags };
