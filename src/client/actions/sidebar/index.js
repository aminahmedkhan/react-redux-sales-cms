export const actions = {
  TOGGLE_SIDEBAR_VISIBILITY: 'TOGGLE_SIDEBAR_VISIBILITY',
  INITIALIZE_STATE: 'INITIALIZE_STATE'
};

export const toggleSidebar = (state) => ({
  type: actions.TOGGLE_SIDEBAR_VISIBILITY
});

export const initializeState = (state) => ({
  type: actions.INITIALIZE_STATE
});
