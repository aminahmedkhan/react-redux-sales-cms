export const actions = {
  ARCHIVE_PROJECT: 'ARCHIVE_PROJECT'
};

export const archiveProject = (data) => ({
  type: actions.ARCHIVE_PROJECT,
  data
});
