import stateGo from 'redux-ui-router/lib/state-go';
import API from '../../shared/api';
import axios from 'axios';

export const actions = {
  SET_USER_ID: 'SET_USER_ID',
  SET_TOKEN: 'SET_TOKEN',
  REGISTER_USER: 'REGISTER_USER'
};

const setToken = (data) => ({
  type: actions.SET_TOKEN,
  data
});

const setUserId = (data) => ({
  type: actions.SET_USER_ID,
  data
});

export const registerUser = (data) => {
  return (dispatch) => {
    axios.post(`${API.url}auth/register`, data)
      .then(({ data: newUser }) => {
        sessionStorage.setItem('token', newUser.token);
        dispatch(setToken(newUser.token));
        dispatch(setUserId(newUser.id));
        // Todo: Dipsatch action to switch navigation bar.
        dispatch(stateGo('dashboard'));
      });
  };
};

export const login = (data) => {
  console.log('data', data);
  return (dispatch) => {
    axios.post(`${API.url}auth/login`, data)
    .then(({ data: user }) => {
      if (user.token && user.id) {
        dispatch(setToken(user.token));
        dispatch(setUserId(user.id));
        sessionStorage.setItem('token', user.token);
        sessionStorage.setItem('userId', user.id);
        dispatch(stateGo('dashboard'));
      } else {
        alert('failed login.');
      }
    });
  };
};

export const logout = () => {
  console.log('loggin out');
  return (dispatch) => {
    axios.get(`${API.url}auth/logout`)
      .then(() => {
        sessionStorage.setItem('token', null);
        sessionStorage.setItem('userId', null);
        dispatch(setToken(null));
        dispatch(setUserId(null));
        dispatch(stateGo('home'));
      });
  };
};
