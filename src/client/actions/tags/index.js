export const actions = {
  ARCHIVE_TAG: 'ARCHIVE_TAG'
};

export const archiveTag = (data) => ({
  type: actions.ARCHIVE_TAG,
  data
});
