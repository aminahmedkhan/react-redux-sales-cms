export const actions = {
  UNIT_DATA_LOADED: 'UNIT_DATA_LOADED',
  LOAD_DEVICES: 'LOAD_DEVICES'
};

export const unitLoadingComplete = (data) => ({
  type: actions.UNIT_DATA_LOADED,
  data
});

export const loadDevices = (data) => ({
  type: actions.LOAD_DEVICES,
  data
});
