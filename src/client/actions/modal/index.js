export const actions = {
  INITIALIZE_STATE: 'INITIALIZE_STATE',
  OPEN_MODAL: 'OPEN_MODAL',
  CLOSE_MODAL: 'CLOSE_MODAL'
};

export const initializeState = () => ({
  type: actions.INITIALIZE_STATE
});

export const openModal = (template) => ({
  type: actions.OPEN_MODAL,
  data: template
});

export const closeModal = () => ({
  type: actions.CLOSE_MODAL
});
