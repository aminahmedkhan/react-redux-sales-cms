import axios from 'axios';
import api from '../../shared/api';

export const actions = {
  ADD_BACKPRESSURE: 'ADD_BACKPRESSURE',
  ADD_CONNECTION_SIZE: 'ADD_CONNECTION_SIZE',
  ADD_CONSTANT_SUPERIMPOSED: 'ADD_CONSTANT_SUPERIMPOSED',
  ADD_VAR_SUPERIMPOSED: 'ADD_VAR_SUPERIMPOSED',
  ADD_DEVICE_NAME: 'ADD_DEVICE_NAME',
  ADD_MEDIA_TYPE: 'ADD_MEDIA_TYPE',
  ADD_ORIFICE_SIZE: 'ADD_ORIFICE_SIZE',
  ADD_SIZING_BASIS: 'ADD_SIZING_BASIS',
  ADD_VALVE_TYPE: 'ADD_VALVE_TYPE',
  DATASHEET_INIT: 'DATASHEET_INIT',
  PROCESS_DATASHEET: 'PROCESS_DATASHEET',
  UPDATE_TEMPLATE: 'UPDATE_TEMPLATE'
};

export const addBackpressure = (options) => ({
  type: actions.ADD_BACKPRESSURE,
  data: options
});

export const addConnectionSize = (options) => ({
  type: actions.ADD_CONNECTION_SIZE,
  data: options
});

export const addConstantSuperimposed = (options) => ({
  type: actions.ADD_CONSTANT_SUPERIMPOSED,
  data: options
});

export const addVarSuperimposed = (options) => ({
  type: actions.ADD_VAR_SUPERIMPOSED,
  data: options
});

export const addMediaType = (options) => ({
  type: actions.ADD_MEDIA_TYPE,
  data: options
});

export const addOrificeSize = (options) => ({
  type: actions.ADD_ORIFICE_SIZE,
  data: options
});

const _addDeviceName = (deviceName) => ({
  type: actions.ADD_DEVICE_NAME,
  data: deviceName
});

export const addDeviceName = (tagId) => {
  return (dispatch) => {
    return axios.get(`${api.url}tags/${tagId}`)
    .then(({ data: tag }) => {
      console.log('tag', tag.Device.name);
      dispatch(_addDeviceName(tag.Device.name));
    })
    .catch((error) => {
      console.log('error', error);
      return error;
    });
  };
};

export const addSizingBasis = (options) => ({
  type: actions.ADD_SIZING_BASIS,
  data: options
});

export const addValveType = (options) => ({
  type: actions.ADD_VALVE_TYPE,
  data: options
});

export const loadDatasheetFields = (data) => ({
  type: actions.DATASHEET_INIT,
  data
});
export const processDatasheet = (units) => ({
  type: actions.PROCESS_DATASHEET,
  data: units
});

export const selectTemplate = (view) => ({
  type: actions.UPDATE_TEMPLATE,
  data: view
});

