import { createSelector } from 'reselect';

const toggleVisibilitySelector = (state) => state.sidebar.visibility;

export const sidebarSelector = createSelector(
  [toggleVisibilitySelector],
  (visibility) => {
    return {
      visibility
    };
  }
);
