import { createSelector } from 'reselect';
import _ from 'lodash';
import { orificeTypeFormulas } from './formulas';
import {
  apiOrificeSizesSelector,
  connectionSizesSelector,
  fullBoreOrificeSizesSelector,
  sizingBasisSelector,
  smallOrificeSizesSelector,
  UnitSelector,
  valveInletXOutletsSelector,
  valveTypesSelector
} from '../../selectors/units';


const filterOrificeSizeHelper = (formula, connectionSizes) => {
  return _.chain(formula)
    .reduce((prev, size) => {
      const connectionSize = _.filter(connectionSizes, (seek) => {
        return size.size === seek.size;
      });
      prev.push(connectionSize);
      return prev;
    }, [])
    .flatten()
    .value();
};


const backPressureSelector = (state) => state.datasheet.backpressure;
const constantSuperImposedSelector = (state) => state.datasheet.constantSuperImposed;
const variableSuperImposedSelector = (state) => state.datasheet.variableSuperImposed;
const currentSizingBasisByIdSelector = (state) => state.datasheet.sizingBasis
const currentTemplateSelector = (state) => state.datasheet.currentTemplate;
const orificeSizeSelector = (state) => state.datasheet.orificeSize;
const orificeTypeSelector = (state) => state.datasheet.orificeType;
const connectionSizeSelector = (state) => state.datasheet.connectionSize;

const filteredOrificeSizesSelector = createSelector(
  [
    orificeTypeSelector,
    apiOrificeSizesSelector,
    smallOrificeSizesSelector,
    fullBoreOrificeSizesSelector
  ],
  (orificeType, directSpring, portable, pilot) => {
    switch (orificeType.name) {
    case 'Direct Spring': {
      return directSpring;
    }
    case 'Portable': {
      return portable;
    }
    case 'Pilot Operated': {
      return pilot;
    }
    default: {
      return orificeType;
    }
    }
  }
);

const filteredConnectionSizesForDirectSpringSelector = createSelector(
  [orificeSizeSelector, connectionSizesSelector, valveInletXOutletsSelector],
  (orificeSize, connectionSizes) => {
    switch (orificeSize.orifice_type) {
    case 'D': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.D, connectionSizes);
    }
    case 'E': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.E, connectionSizes);
    }
    case 'F': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.F, connectionSizes);
    }
    case 'G': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.G, connectionSizes);
    }
    case 'H': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.H, connectionSizes);
    }
    case 'J': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.J, connectionSizes);
    }
    case 'K': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.K, connectionSizes);
    }
    case 'L': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.L, connectionSizes);
    }
    case 'M': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.M, connectionSizes);
    }
    case 'N': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.N, connectionSizes);
    }
    case 'P': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.P, connectionSizes);
    }
    case 'Q': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.Q, connectionSizes);
    }
    case 'R': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.R, connectionSizes);
    }
    case 'T': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.T, connectionSizes);
    }
    case 'T2': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.T2, connectionSizes);
    }
    case 'V': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.V, connectionSizes);
    }
    case 'W': {
      return filterOrificeSizeHelper(orificeTypeFormulas.directSpring.W, connectionSizes);
    }
    default: {
      return orificeSize;
    }
    }
  }
);

const filteredConnectionSizesForPortableSelector = createSelector(
  [orificeSizeSelector, connectionSizesSelector],
  (orificeSize, connectionSizes) => {
    switch (orificeSize.orifice_type) {
    case '5': {
      return filterOrificeSizeHelper(orificeTypeFormulas.portable.sizeFive, connectionSizes);
    }
    case '6': {
      return filterOrificeSizeHelper(orificeTypeFormulas.portable.sizeSix, connectionSizes);
    }
    case '7': {
      return filterOrificeSizeHelper(orificeTypeFormulas.portable.sizeSeven, connectionSizes);
    }
    case '8': {
      return filterOrificeSizeHelper(orificeTypeFormulas.portable.sizeEight, connectionSizes);
    }
    case '9': {
      return filterOrificeSizeHelper(orificeTypeFormulas.portable.sizeNine, connectionSizes);
    }
    default: {
      return orificeSize;
    }
    }
  }
);

const filteredConnectionSizesSelector = createSelector(
  [
    orificeTypeSelector,
    connectionSizesSelector,
    filteredConnectionSizesForDirectSpringSelector,
    filteredConnectionSizesForPortableSelector
  ],
  (orificeType, connectionSizes, directSpringValveFilter, portableValveFilter) => {
    switch (orificeType.name) {
    case 'Direct Spring': {
      return directSpringValveFilter;
    }
    case 'Portable': {
      return portableValveFilter;
    }
    case 'Pilot Operated': {
      return filterOrificeSizeHelper(orificeTypeFormulas.portable.sizeSeven, connectionSizes);
    }
    default: {
      return orificeType;
    }
    }
  }
);

const processData = (collection, connectionSize) => {
  return _.chain(collection)
  .reduce((prev, size) => {
    if (size.size === connectionSize.size) {
      prev.push(size);
    }
    return prev;
  }, [])
  .flatten()
  .value();
};

const filterValveInletOutletSizesSelector = createSelector(
  [orificeSizeSelector, connectionSizeSelector],
  (orificeSize, connectionSize) => {
    switch (orificeSize.orifice_type) {
    case 'D': {
      return processData(orificeTypeFormulas.directSpring.D, connectionSize);
    }
    case 'E': {
      return processData(orificeTypeFormulas.directSpring.E, connectionSize);
    }
    case 'F': {
      return processData(orificeTypeFormulas.directSpring.F, connectionSize);
    }
    case 'G': {
      return processData(orificeTypeFormulas.directSpring.G, connectionSize);
    }
    case 'H': {
      return processData(orificeTypeFormulas.directSpring.H, connectionSize);
    }
    case 'J': {
      return processData(orificeTypeFormulas.directSpring.J, connectionSize);
    }
    case 'K': {
      return processData(orificeTypeFormulas.directSpring.K, connectionSize);
    }
    case 'L': {
      return processData(orificeTypeFormulas.directSpring.L, connectionSize);
    }
    case 'M': {
      return processData(orificeTypeFormulas.directSpring.M, connectionSize);
    }
    case 'N': {
      return processData(orificeTypeFormulas.directSpring.N, connectionSize);
    }
    case 'P': {
      return processData(orificeTypeFormulas.directSpring.P, connectionSize);
    }
    case 'Q': {
      return processData(orificeTypeFormulas.directSpring.Q, connectionSize);
    }
    case 'R': {
      return processData(orificeTypeFormulas.directSpring.R, connectionSize);
    }
    case 'T': {
      return processData(orificeTypeFormulas.directSpring.T, connectionSize);
    }
    case 'T2': {
      return processData(orificeTypeFormulas.directSpring.T2, connectionSize);
    }
    case 'V': {
      return processData(orificeTypeFormulas.directSpring.V, connectionSize);
    }
    case 'W': {
      return processData(orificeTypeFormulas.directSpring.W, connectionSize);
    }
    default: {
      return orificeSize;
    }
    }
  }
);

// TODO: Verify standard names between orifice / device / valve.
export const deviceNameSelector = (state) => state.datasheet.deviceName;
export const deviceImageSelector = createSelector(
  [orificeTypeSelector],
  (orificeType) => {
    console.log('orificeType', orificeType);
    console.log('orificeType.id', orificeType.id);
    switch (orificeType.id) {
    case 1: {
      return 'assets/valves/direct_spring_valve.jpg';
    }
    case 2: {
      return 'assets/valves/portable_valve.gif';
    }
    case 3: {
      return 'assets/valves/pilot_valve.jpg';
    }
    default: {
      return '';
    }
    }
  }
);

export const overpressureSelector = createSelector(
  [sizingBasisSelector, currentSizingBasisByIdSelector],
  (sizingBasisTable, sizingBasisId) => {
    return sizingBasisTable[ sizingBasisId ].overpressure;
  }
);

export const totalBackPressureSelector = createSelector(
  [
    backPressureSelector,
    variableSuperImposedSelector,
    constantSuperImposedSelector
  ],
  (backpressure, varSuperImposed, constantSuperImposed) => {
    return (Number(backpressure) + Number(varSuperImposed) + Number(constantSuperImposed));
  }
);

export const datasheetSelector = createSelector(
  [
    UnitSelector,
    filteredOrificeSizesSelector,
    valveTypesSelector,
    filteredConnectionSizesSelector,
    filterValveInletOutletSizesSelector,
    currentTemplateSelector
  ],
  (units = {}, orificeSizes = {}, valves, connectionSizes, ixoSizes, currentTemplate) => {
    return {
      units,
      orificeSizes,
      valves,
      connectionSizes,
      ixoSizes,
      currentTemplate
    };
  }
);
