export const orificeTypeFormulas = {
  directSpring: {
    D: [
        { size: '1 x 2',
          ixoValves: [{
            rating: '150 x 150'
          }, {
            rating: '300LT x 150'
          }, {
            rating: '300 x 150'
          }, {
            rating: '600 x 150'
          }]
        },
      {
        size: '1-1/2 x 2',
        ixoValves: [{
          rating: '900 x 300'
        }, {
          rating: '1500 x 300'
        }]
      },
      {
        size: '1-1/2 x 3',
        ixoValves: [{
          rating: '2500 x 300'
        }]
      }
    ],
    E: [
      {
        size: '1 x 2',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }]
      },
      {
        size: '1-1/2 x 2',
        ixoValves: [{
          rating: '900 x 300'
        }, {
          rating: '1500 x 300'
        }]
      },
      {
        size: '1-1/2 x 3',
        ixoValves: [{
          rating: '2500 x 300'
        }]
      }
    ],
    F: [
      {
        size: '1-1/2 x 2',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }]
      },
      {
        size: '1-1/2 x 3',
        ixoValves: [{
          rating: '900 x 300'
        }, {
          rating: '1500 x 300'
        }, {
          rating: '2500 x 300'
        }]
      }
    ],
    G: [
      {
        size: '1-1/2 x 3',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }, {
          rating: '900 x 300'
        }]
      },
      {
        size: '2 x 3',
        ixoValves: [{
          rating: '1500 x 300'
        }, {
          rating: '2500 x 300'
        }]
      }
    ],
    H: [
      {
        size: '1-1/2 x 3',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }]
      },
      {
        size: '2 x 3',
        ixoValves: [{
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }, {
          rating: '900 x 150'
        }, {
          rating: '1500 x 300'
        }]
      }
    ],
    J: [
      {
        size: '2 x 3',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }]
      },
      {
        size: '3 x 4',
        ixoValves: [{
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }, {
          rating: '900 x 150'
        }, {
          rating: '1500 x 300'
        }]
      }
    ],
    K: [
      {
        size: '3 x 4',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }, {
          rating: '900 x 150'
        }, {
          rating: '1500 x 300'
        }]
      }
    ],
    L: [
      {
        size: '3 x 4',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }]
      },
      {
        size: '4 x 6',
        ixoValves: [{
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }, {
          rating: '900 x 150'
        }, {
          rating: '1500 x 300'
        }]
      }
    ],
    M: [
      {
        size: '4 x 6',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }, {
          rating: '900 x 150'
        }]
      }
    ],
    N: [
      {
        size: '4 x 6',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }]
      }
    ],
    P: [
      {
        size: '4 x 6',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }, {
          rating: '900 x 150'
        }]
      }
    ],
    Q: [
      {
        size: '6 x 8',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }]
      }
    ],
    R: [
      {
        size: '6 x 8',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }, {
          rating: '600 x 150'
        }]
      }
    ],
    T: [
      {
        size: '8 x 10',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }]
      }
    ],
    T2:
      [{
        size: '8 x 10',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }]
      }
    ],
    V: [
      {
        size: '10 x 14',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }]
      }
    ],
    W: [
      {
        size: '12 x 16',
        ixoValves: [{
          rating: '150 x 150'
        }, {
          rating: '300LT x 150'
        }, {
          rating: '300 x 150'
        }]
      }
    ]
  },

  portable: {
    sizeFive: [{ size: '1/2 x 1' }, { size: '3/4 x 1' }, { size: '1 x 1' }],
    sizeSix: '',
    sizeSeven: '',
    sizeEight: '',
    sizeNine: ''
  }
};
