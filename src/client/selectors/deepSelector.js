import { createSelectorCreator, defaultMemoize } from 'reselect';
import { is } from 'immutable';

/**
 * For any selector that is grabbing from the Redux store, should be created
 * using this createDeepSelector function.
 */
const createDeepSelector = createSelectorCreator(
  defaultMemoize
);

export default createDeepSelector;
