import { actions } from '../../actions/tags';

const archiveTag = (state, data) => {
  console.log('archive tag logic goes here. Id:', data);
  return state;
};

export default function reducer(state = {}, { type, data }) {
  switch (type) {
  case actions.ARCHIVE_TAG: {
    return archiveTag(state, data);
  }
  default: {
    return state;
  }
  }
}
