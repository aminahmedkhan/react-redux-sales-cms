import { actions } from '../../actions/user';

const setUserId = (state, data) => {
  return {
    ...state,
    id: data
  };
};

const setToken = (state, data) => {
  return {
    ...state,
    token: data
  };
};

export default function reducer(state = {}, { data, type }) {
  switch (type) {
  case actions.SET_USER_ID: {
    return setUserId(state, data);
  }
  case actions.SET_TOKEN: {
    return setToken(state, data);
  }
  default:
    return state;
  }
}
