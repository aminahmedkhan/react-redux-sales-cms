import { actions } from '../../actions/units';

const loadDevices = (state, data) => {
  return {
    ...state,
    devices: data
  };
};

export default function reducer(state = {}, { data, type }) {
  switch (type) {
  case actions.LOAD_DEVICES: {
    return loadDevices(state, data);
  }
  default:
    return state;
  }
}
