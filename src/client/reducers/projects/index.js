import { actions } from '../../actions/projects';
import stateGo from 'redux-ui-router/lib/state-go';

const archiveProject = (state, data) => {
  console.log('archive project logic here.  ID:', data);
  return state;
};

export default function reducer(state = {}, { type, data }) {
  switch (type) {
  case actions.ARCHIVE_PROJECT: {
    return archiveProject(state, data);
  }
  default: {
    return state;
  }
  }
}
