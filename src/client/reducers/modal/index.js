import { actions } from '../../actions/modal';
import $ from 'jquery';

const fadeIn = () => {
  setTimeout(() => {
    $('#activeModal').addClass('fade-in');
  });
};

const openModal = (state, data) => {
  fadeIn();
  return {
    ...state,
    template: data
  };
};

const closeModal = (state) => {
  return {
    ...state,
    template: null
  };
};

export default function reducer(state = {}, { data, type }) {
  switch (type) {
  case actions.INITIALIZE_STATE: {
    return {
      ...state,
      template: ''
    };
  }
  case actions.OPEN_MODAL: {
    return openModal(state, data);
  }
  case actions.CLOSE_MODAL: {
    return closeModal(state, data);
  }
  default: {
    return state;
  }
  }
}
