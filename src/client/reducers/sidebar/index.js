import { actions } from '../../actions/sidebar';

const init = (state, action) => {
  return {
    ...state,
    visibility: 'closed'
  };
}

const toggleVisiblityFilter = (state, action) => {
  const visibility = (state.visibility === 'open') ? 'closed' : 'open'; 
  return {
    ...state,
    visibility
  }
};

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
  case actions.TOGGLE_SIDEBAR_VISIBILITY: {
    return toggleVisiblityFilter(state, action);
  }
  case actions.INITIALIZE_STATE: {
    return init(state, action);
  }
  default: {
    return state;
  }
  }
}
