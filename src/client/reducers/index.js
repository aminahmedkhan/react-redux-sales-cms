import { combineReducers } from 'redux';
import { router } from 'redux-ui-router';
import datasheet from './datasheet';
import sidebar from './sidebar';
import modal from './modal';
import projects from './projects';
import tags from './tags';
import units from './units';
import user from './user';

const rootReducer = combineReducers({
  datasheet,
  sidebar,
  projects,
  modal,
  tags,
  router,
  units,
  user
});

export default rootReducer;
