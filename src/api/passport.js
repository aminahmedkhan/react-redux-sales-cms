/*jslint es6, node: true */

(function () {
    "use strict";

    const passport = require('passport');
    const LocalStrategy = require('passport-local').Strategy;
    const bcrypt = require('bcrypt');
    const models = require("./models");
    const User = models.User;
    const Promise = require("bluebird");

    const setup = function (app) {
        app.use(passport.initialize());
        app.use(passport.session());

        passport.serializeUser(function (user, done) {
            done(null, user);
        });

        passport.deserializeUser(function (obj, done) {
            done(null, obj);
        });

        function localStrategy(username, password, done) {
            process.nextTick(function () {
                User.findOne({
                    where: {username: username}
                }).then(function (user) {

                    if (!user) {
                        return done(null, false, {message: 'Incorrect username.'});
                    }

                    return Promise.promisify(bcrypt.compare)(password, user.password)
                        .then(function (isValidLoginCredentials) {
                            if (!isValidLoginCredentials) {
                                return done(null, false, {message: 'Incorrect password.'});
                            }

                            return done(null, user);
                        });

                }).catch(function (err) {
                    console.error(err);

                    return done(null, false, {message: 'Incorrect credentials.'});
                });
            });
        }

        passport.use(new LocalStrategy(localStrategy));
    };

    module.exports = {setup};

}());