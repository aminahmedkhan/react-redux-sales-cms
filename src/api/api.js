(() => {
    "use strict";
    const express = require('express');
    const app = express();
    const apiRouter = require('./api-router');
    const cors = require('cors');
    const db = require('./models');
    const bodyParser = require('body-parser');
    const passport = require('./passport.js');

    app.use(cors({
        origin: 'http://localhost:8080'
    }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: false
    }));

    passport.setup(app);
    app.use('/api', apiRouter);

    const server = app.listen(process.env.app_port || 3000, function () {
        const host = server.address().address;
        const port = server.address().port;

        console.log('Listening at http://%s:%s', host, port);
        db.sequelize.sync();
    });
}());