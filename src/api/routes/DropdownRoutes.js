/*jslint es6, node: true */
(function () {
    "use strict";

    const express = require('express');
    const router = express.Router();
    const Dropdown = require('../utilities/dropdown.js');

    router.get('/', function (ignore, res) {
        return Dropdown.getAllDropdownValues()
            .then(function (dropdownValues) {
                res.json({
                    apiOrificeSizes: dropdownValues.apiOrificeSizes,
                    asmeSizings: dropdownValues.asmeSizings,
                    backFlowPreventers: dropdownValues.backFlowPreventers,
                    bodyMaterials: dropdownValues.bodyMaterials,
                    capTypes: dropdownValues.capTypes,
                    codeTypes: dropdownValues.codeTypes,
                    connectionSizes: dropdownValues.connectionSizes,
                    devices: dropdownValues.devices,
                    fieldTestConnections: dropdownValues.fieldTestConnections,
                    flangeFacings: dropdownValues.flangeFacings,
                    requiredFlowCapacityUnits: dropdownValues.requiredFlowCapacityUnits,
                    fullBoreOrificeSizes: dropdownValues.fullBoreOrificeSizes,
                    mediaTypes: dropdownValues.mediaTypes,
                    pressureUnits: dropdownValues.pressureUnits,
                    seatMaterials: dropdownValues.seatMaterials,
                    sensings: dropdownValues.sensings,
                    sizingBasis: dropdownValues.sizingBasis,
                    smallOrificeSizes: dropdownValues.smallOrificeSizes,
                    spikeSnubbers: dropdownValues.spikeSnubbers,
                    springMaterials: dropdownValues.springMaterials,
                    supplyFilters: dropdownValues.supplyFilters,
                    tempUnits: dropdownValues.tempUnits,
                    trimMaterials: dropdownValues.trimMaterials,
                    valveInletXOutlets: dropdownValues.valveInletXOutlets,
                    valveTypes: dropdownValues.valveTypes,
                    viscosityUnits: dropdownValues.viscosityUnits
                });
            });
    });

    router.get('/api-orifice-sizes', function (ignore, res) {
        Dropdown.getOrificeSizes()
            .then(function (apiOrificeSizes) {
                res.json(apiOrificeSizes);
            });
    });

    router.get('/asme-sizings', function (ignore, res) {
        Dropdown.getAsmeSizings()
            .then(function (asmeSizings) {
                res.json(asmeSizings);
            });
    });

    router.get('/back-flow-preventers', function (ignore, res) {
        Dropdown.getBackFlowPreventers()
            .then(function (backFlowPreventers) {
                res.json(backFlowPreventers);
            });
    });

    router.get('/body-materials', function (ignore, res) {
        Dropdown.getBodyMaterials()
            .then(function (bodyMaterials) {
                res.json(bodyMaterials);
            });
    });

    router.get('/cap-types', function (ignore, res) {
        Dropdown.getCapTypes()
            .then(function (capTypes) {
                res.json(capTypes);
            });
    });

    router.get('/connection-sizes', function (ignore, res) {
        Dropdown.getConnectionSizes()
            .then(function (connectionSizes) {
                res.json(connectionSizes);
            });
    });

    router.get('/devices', function (ignore, res) {
        Dropdown.getDevices()
            .then(function (devices) {
                res.json(devices);
            });
    });

    router.get('/field-test-connections', function (ignore, res) {
        Dropdown.getFieldTestConnections()
            .then(function (fieldTestConnections) {
                res.json(fieldTestConnections);
            });
    });

    router.get('/flange-facings', function (ignore, res) {
        Dropdown.getFlangeFacings()
            .then(function (flangeFacings) {
                res.json(flangeFacings);
            });
    });

    router.get('/full-bore-orifice-sizes', function (ignore, res) {
        Dropdown.getFullBoreOrificeSizes()
            .then(function (fullBoreOrificeSizes) {
                res.json(fullBoreOrificeSizes);
            });
    });

    router.get('/pressure-units', function (ignore, res) {
        Dropdown.getPressureUnits()
            .then(function (units) {
                res.json(units);
            });
    });

    router.get('/required-flow-capacity-units', function (ignore, res) {
        Dropdown.getRequiredFlowCapacityUnits()
            .then(function (units) {
                res.json(units);
            });
    });

    router.get('/seat-materials', function (ignore, res) {
        Dropdown.getSeatMaterials()
            .then(function (seatMaterials) {
                res.json(seatMaterials);
            });
    });

    router.get('/sensings', function (ignore, res) {
        Dropdown.getSensings()
            .then(function (sensings) {
                res.json(sensings);
            });
    });

    router.get('/sizing-basis-units', function (ignore, res) {
        Dropdown.getSizingBasisUnits()
            .then(function (units) {
                res.json(units);
            });
    });

    router.get('/small-orifice-sizes', function (ignore, res) {
        Dropdown.getSmallOrificeSizes()
            .then(function (smallOrificeSizes) {
                res.json(smallOrificeSizes);
            });
    });

    router.get('/spike-snubbers', function (ignore, res) {
        Dropdown.getSpikeSnubbers()
            .then(function (spikeSnubbers) {
                res.json(spikeSnubbers);
            });
    });

    router.get('/spring-material', function (ignore, res) {
        Dropdown.getSpringVariables()
            .then(function (springMaterials) {
                res.json(springMaterials);
            });
    });

    router.get('/supply-filters', function (ignore, res) {
        Dropdown.getSupplyFilters()
            .then(function (supplyFilters) {
                res.json(supplyFilters);
            });
    });

    router.get('/temperature-units', function (ignore, res) {
        Dropdown.getTemperatureUnits()
            .then(function (units) {
                res.json(units);
            });
    });

    router.get('/trim-materials', function (ignore, res) {
        Dropdown.getTrimMaterials()
            .then(function (trimMaterials) {
                res.json(trimMaterials);
            });
    });

    router.get('valve-inlet-x-outlets', function (ignore, res) {
        Dropdown.getValveInletXOutlets()
            .then(function (valveInletXOutlets) {
                res.json(valveInletXOutlets);
            });
    });

    router.get('/valve-types', function (ignore, res) {
        Dropdown.getValveTypes()
            .then(function (valveTypes) {
                res.json(valveTypes);
            });
    });

    router.get('/viscosity-units', function (ignore, res) {
        Dropdown.getViscosityUnits()
            .then(function (units) {
                res.json(units);
            });
    });

    module.exports = router;
}());