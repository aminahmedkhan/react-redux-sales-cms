/*jslint es6, node: true */

(function () {
    'use strict';
    const express = require('express');
    const router = express.Router();
    const AuthActions = require('../utilities/authorization.js');
    const passport = require('passport');
    const jwt = require('jsonwebtoken');
    const tokenSecret = "sushisecret";

    router.get('/isAuthenticated', function (req, res) {
        const token = req.headers.authorization;

        jwt.verify(token, tokenSecret, function (ignore, decoded) {
            if (decoded) {
                res.status(200).send({
                    status: 200,
                    token});
            } else {
                res.sendStatus(401);
            }
        });
    });

    router.post('/login', function (req, res, next) {
        passport.authenticate('local', function (err, user, ignore) {
            if (err) {
                return next(err);
            }

            if (user === false) {
                res.jsonp({
                    statusCode: 401,
                    message: 'Invalid Credentials',
                    error: 'Unauthorized'
                });
            } else {
                const payload = {
                    iss: 'sizeselect.com',
                    iat: Date.now()
                };

                const id = user.id;
                let token = jwt.sign({payload: payload}, 'sushisecret');
                const response = {
                    token,
                    id
                };

                res.status(200).json(response);
            }
        })(req, res, next);
    });

    router.get('/logout', function (req, res) {
        res.header('Bearer', '');
        req.logout();
        res.json({
            status: 200,
            message: 'Logout successful'
        });
    });

    router.post('/register', function (req, res) {
        const spec = req.body;

        AuthActions.registerUser(spec)
            .then(function (response) {
                res.status(201).json(response);
            })
            .catch({code: 401}, function (error) {
                console.error("401 error", error);
                res.json({error});
            })
            .catch({code: 404}, function (error) {
                console.error("404 error", error);
                res.json({error});
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    module.exports = router;
}());