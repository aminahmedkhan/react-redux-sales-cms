/*jslint es6, node: true */

(function () {
    "use strict";

    const express = require('express');
    const router = express.Router();
    const ProjectActions = require('../utilities/project.js');
    const jwt = require('jsonwebtoken');

    const verify = function (req, res, next) {
        jwt.verify(req.headers.authorization, 'sushisecret', function (err, decoded) {
            if (decoded) {
                next();
            } else {
                res.json({
                    error: 'Unauthorized',
                    status: 401,
                    message: 'Invalid or expired access token.'
                });
            }
        });
    };

    // router.use('/', verify)

    router.get('/', function (req, res) {
        const load = {
            userId: req.query.userId
        };

        ProjectActions.getProjectsByUserId(load)
            .then(function (projects) {
                res.json(projects);
            });
    });

    router.get('/:id', function (req, res) {
        const load = {
            projectId: req.params.id
        };

        ProjectActions.getProjectById(load)
            .then(function (project) {
                res.json(project);
            });
    });

    router.post('/', function (req, res) {
        const body = req.body;
        const load = {
            name: body.name,
            company: body.company,
            location: body.location,
            userId: body.user_id
        };

        ProjectActions.createProject(load)
            .then(function (project) {
                res.json(project);
            })
            .catch(function (err) {
                res.json({error: err});
            });
    });

    router.put('/:id/archive', function (req, res) {
        const load = {
            projectId: req.params.id
        };

        ProjectActions.archiveProject(load)
            .then(function () {
                res.sendStatus(201);
            })
            .catch(function () {
                res.sendStatus(404);
            });
    });

    module.exports = router;
}());