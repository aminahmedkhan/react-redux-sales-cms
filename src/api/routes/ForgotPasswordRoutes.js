/*jslint es6, node: true */

(function () {
    "use strict";

    const express = require('express');
    const router = express.Router();
    const Password = require('../utilities/password.js');

    router.put('/', function (req, res) {
        const load = {
            email: req.body.email
        };

        Password.createResetToken(load)
            .then(function () {
                res.sendStatus(201);
            })
            .catch(function (error) {
                console.log("error", error);
                res.sendStatus(error.status);
            });
    });

    module.exports = router;
}());