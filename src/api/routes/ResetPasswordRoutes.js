/*jslint es6, node: true */
(function () {
    'use strict';

    const express = require('express');
    const router = express.Router();
    const Password = require('../utilities/password.js');


    router.get('/', function (req, res) {
        const load = {
            resetPWToken: req.query.token
        };

        Password.getUser(load)
            .then(function (userPWInfo) {
                res.json(userPWInfo);
            })
            .catch(function (error) {
                res.sendStatus(error.status);
            });
    });

    router.put('/', function (req, res) {
        const load = {
            resetPWToken: req.body.token,
            password: req.body.password
        };

        Password.resetUserPassword(load)
            .then(function () {
                res.sendStatus(201);
            })
            .catch(function (error) {
                console.log("error", error);
                res.sendStatus(error.status);
            });
    });

    module.exports = router;
}());