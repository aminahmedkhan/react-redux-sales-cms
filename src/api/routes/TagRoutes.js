/*jslint es6, node: true */
(function () {
    "use strict";

    const express = require('express');
    const router = express.Router();
    const TagActions = require('../utilities/tag.js');

    router.get('/', function (req, res) {
        const projectId = req.query.projectId;

        TagActions.getTags({projectId})
            .then(function (tags) {
                res.json(tags);
            })
            .catch({code: 404}, function (error) {
                console.error("404 error", error);
                res.json({error});
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    router.get('/:id', function (req, res) {
        const tagId = req.params.id;

        TagActions.getTagById({tagId})
            .then(function (tags) {
                res.json(tags);
            })
            .catch({code: 404}, function (error) {
                console.error("404 error", error);
                res.json({error});
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    router.post('/', function (req, res) {
        const tagData = req.body;

        TagActions.create(tagData)
            .then(function (tagSheet) {
                res.json(tagSheet);
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    router.get('/:id/tag-sheet', function (req, res) {
        const tagId = req.params.id;

        TagActions.getTagSheet({tagId})
            .then(function (tagSheet) {
                res.json(tagSheet);
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    router.put('/:id/tag-sheet', function (req, res) {
        const spec = {
            tagId: req.params.id,
            tagData: req.body
        };

        TagActions.updateTagsheet(spec)
            .then(function (tagSheet) {
                res.status(201).send(tagSheet);
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    router.get('/:id/data-sheet', function (req, res) {
        const spec = {
            tagId: req.params.id
        };

        TagActions.getDataSheet(spec)
            .then(function (dataSheet) {
                res.json(dataSheet);
            })
            .catch({code: 404}, function (error) {
                console.error("404 error", error);
                res.json({error});
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    router.put('/:id/archive', function (req, res) {
        const spec = {
            tagId: req.params.id
        };

        TagActions.archiveTag(spec)
            .then(function () {
                res.sendStatus(201);
            })
            .catch({code: 404}, function (error) {
                console.error("404 error", error);
                res.json({error});
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    module.exports = router;
}());