/*jslint es6, node: true */

(function () {
    "use strict";

    const express = require('express');
    const router = express.Router();
    const UserActions = require('../utilities/user.js');

    router.get('/:id', function (req, res) {
        const load = {
            userId: req.params.id
        };

        UserActions.getUser(load)
            .then(function (user) {
                res.json(user);
            })
            .catch({code: 404}, function (error) {
                console.error("404 error", error);
                res.json({error});
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    router.put('/:id/profile-updates', function (req, res) {
        const load = {
            userId: req.params.id,
            body: req.body
        };

        UserActions.updateProfile(load)
            .then(function () {
                res.sendStatus(201);
            })
            .catch({code: 400}, function (error) {
                console.error("400 error", error);
                res.json({error});
            })
            .catch({code: 422}, function (error) {
                console.error("422 error", error);
                res.json({error});
            })
            .catch(function (error) {
                console.error("Catch all error", error);
                res.json({error});
            });
    });

    module.exports = router;
}());