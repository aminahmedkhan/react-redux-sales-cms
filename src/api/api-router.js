/*jslint es6, node: true */

(function () {
    "use strict";

    const express = require('express');
    const api = express.Router();
    const auth = require('./routes/AuthorizationRoutes');
    const user = require('./routes/UserRoutes');
    const project = require('./routes/ProjectRoutes');
    const forgotPassword = require('./routes/ForgotPasswordRoutes');
    const resetPassword = require('./routes/ResetPasswordRoutes');
    const tags = require('./routes/TagRoutes');
    const dropdown = require('./routes/DropdownRoutes');
    const Authorize = require('./utilities/authorization.js');

    api.use('/auth', auth);
    api.use('/forgot-password', forgotPassword);
    api.use('/reset-password', resetPassword);
    api.use('/users', user);
    api.use('/projects', project);
    api.use('/tags', tags);
    api.use('/dropdowns', dropdown);

    module.exports = api;
}());